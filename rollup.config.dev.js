import pkg from './package.json';
import svelte from 'rollup-plugin-svelte';
import livereload from 'rollup-plugin-livereload';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import postcss from 'svelte-preprocess-postcss';
import buble from 'rollup-plugin-buble';

const name = pkg.name;

export default {
  input: 'example/js/main.js',
  output: { file: 'example/dist/bundle.js', 'format': 'umd', name },
  plugins: [
    svelte({
      preprocess: {
        style: postcss({
          useConfigFile: false,
          plugins: [
            require('postcss-nested'),
          ]
        })
      }
    }),
    resolve(),
    commonjs(),
    buble(),
    livereload(),
  ]
};
