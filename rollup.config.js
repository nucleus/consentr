import pkg from './package.json';
import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import postcss from 'svelte-preprocess-postcss'
import { terser } from 'rollup-plugin-terser';
import buble from 'rollup-plugin-buble';

const name = pkg.name;

export default [
  {
    input: 'src/index.js',
    output: [
      { file: pkg.module, 'format': 'es' },
      { file: pkg.main, 'format': 'umd', name }
    ],
    plugins: [
        svelte({
          cascade: false,
          store: true,
          preprocess: {
            style: postcss({
              useConfigFile: false,
              plugins: [
                require('postcss-nested'),
              ]
            })
          },
        }),

        resolve(),
        commonjs(),
        buble(),
        terser()
    ]
  }
];
