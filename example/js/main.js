import consentr from '../../src';
import { Settings } from '../../src';

consentr({
  permissions: [
    {
      name: 'required',
      description: '<h2>Required</h2><p>Lorem ipsum dolor sit amet.</p>',
      required: true
    },
    {
      name: 'statistics',
      description: '<h2>Statistics</h2><p>Lorem ipsum dolor sit amet.</p>'
    },
    {
      name: 'marketing',
      description: '<h2>Marketing</h2><p>Lorem ipsum dolor sit amet.</p>'
    },
  ],
  notification: {
    title: 'Informasjonskapsler',
    description: 'Vi bruker informasjonskapsler til statistikk, optimalisering av innhold og tilpasset markedsføring på andre nettsider.',
    settingsText: 'Flere valg',
    acceptText: 'Jeg godtar',
    settingsPage: '/settings'
  },
  type: "large"
})

const settingsElm = document.getElementById("cookie-settings");

if(settingsElm) {
  new Settings({
    target: settingsElm,
    data: {
      labels: ['on', 'off']
    }
  })
}
