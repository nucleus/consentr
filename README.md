# consentr

Simple cookie manager to be used with Google Tag Manager. Includes a simple notification and settings panel.

## Example

Define permission types that the user will be able to opt in and out of, these needs to be set up as custom triggers in Google Tag Manager as well (see below).

```javascript
import consentr from 'consentr';

consentr({
  // permission types, description is shown in the settings panel
  permissions: [
    {
      name: 'marketing',
      description: '<h2>Marketing</h2><p>Lorem ipsum dolor sit amet.</p>'
    },
    ...
  ],
  // notification settings: settingsPage is where you'd like to show the settings panel
  notification: {
    title: 'Informasjonskapsler',
    description: 'Vi bruker informasjonskapsler til statistikk, optimalisering av innhold og tilpasset markedsføring på andre nettsider.',
    settingsText: 'Innstillinger',
    acceptText: 'Jeg forstår',
    settingsPage: '/informasjonskapsler'
  }
})
```

Show the settings panel where you want:

```javascript
import { Settings } from 'consentr'

new Settings({
  target: document.getElementById("cookie-settings")
})
```

## Google Tag Manger

Every permission type needs a corresponding custom trigger in Google Tag Manager.

Granted permissions is stored in the `_consentr_permissions` cookie so we'll use that to check if the user has given permission to whatever we want to trigger.

### Let's use "marketing" as an example

Create a "Custom Event" trigger that matches any event (use regex) like so:

![Example trigger](/assets/gtm_trigger.png?raw=true)

Since Google Tag Manager only suports "OR" when using multiple events on triggers, we'll need to check for when the permission doesn't exist in the cookie and use it as an exception trigger on our tag. 

Now we can add "Marketing Disabled" as an trigger exception which will prevent the tag from firing if "marketing" is absent from the cookie.

![Example tag](/assets/gtm_tag.png?raw=true)

You can also listen for the `permissions.accepted` event for when the user clicks the accept button in the notification as seen in the example screenshot above.
