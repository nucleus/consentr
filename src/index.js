import cookies from "js-cookie";
import { setPermissions, grantPermission } from "./permissions";
import Notification from "./components/Notification.html";
import FullscreenNotification from "./components/FullscreenNotification.html";

export { default as Settings } from "./components/Settings.html";

const types = {
  small: Notification,
  large: FullscreenNotification,
};

export default ({ permissions = [], notification, type = "small" }) => {
  setPermissions(permissions);

  if (typeof cookies.get("_consentr_notification_disabled") === "undefined") {
    const n = new types[type]({
      target: document.body,
      data: notification,
    });

    n.on("accepted", () => {
      permissions.forEach((p) => grantPermission(p));

      window.dataLayer.push({
        event: "permissions.accepted",
        url: window.location.href,
      });

      cookies.set("_consentr_notification_disabled", 1, {
        expires: 365,
      });
    });

    n.on("settings", () => {
      cookies.set("_consentr_notification_disabled", 1, {
        expires: 365,
      });
    });
  }
};
