import cookies from "js-cookie";

// permission details with name and description etc.
const permissions = [];

const savePermissions = () => {
  cookies.set(
    "_consentr_permissions",
    JSON.stringify(permissions.filter(p => p.granted).map(p => p.name)),
    {
      expires: 365,
      path: '/'
    }
  );
};

export function setPermissions(newPermissions) {
  let grantedPermissions = JSON.parse(
    cookies.get("_consentr_permissions") || "[]"
  );

  newPermissions = newPermissions.map(permission => {
    permission["granted"] =
      grantedPermissions.some(
        permissionName => permissionName === permission.name
      ) || permission.required;
    return permission;
  });

  permissions.push(...newPermissions);
}

export function getPermissions() {
  return permissions;
}

export function grantPermission(permission) {
  permission["granted"] = true;
  savePermissions();
}

export function denyPermission(permission) {
  permission["granted"] = false;
  savePermissions();
}
